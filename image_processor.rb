require 'mini_magick'

class ImageProcessor
  def self.apply file, format
    res = MiniMagick::Image.open file
    if format[format.size - 1] == "_" #analog of dragonfly thumb('100x100#')
      base_width = res["width"]
      base_height = res["height"]
      final_width, final_height = format.split("x").map &:to_i
      scale = [final_height.to_f/base_height, final_width.to_f/base_width].max
      resize_format = "#{base_width * scale}x#{base_height * scale}"
      res.resize resize_format
      resized_width = res["width"]
      resized_height = res["height"]
      crop_start = {
          x: (0.5 * (resized_width - final_width)).to_i,
          y: (0.5 * (resized_height - final_height)).to_i
      }
      res.crop "#{final_width}x#{final_height}+#{crop_start[:x]}+#{crop_start[:y]}"
      res
    else
      res.resize format
      res
    end
    res
  end
end