require "fileutils"

def start
  public_dir = File.join File.dirname(__FILE__), "public"
  backup_dir = File.join File.dirname(__FILE__), "public_backup_#{rand(10000)}"
  FileUtils::cp_r public_dir, backup_dir
  projects = search_children public_dir
  projects.each do |project|
    client_keys = search_children project
    client_keys.each do |client_key|
      images = search_children client_key
      images.each do |image|
        update_structure image
      end
    end
  end
  puts "done..."
end

def update_structure image
  puts "old image path: #{image}"
  key = File.basename(image).split(".")[0]
  new_image_dir = File.join(File.dirname(image), key)
  new_image_path =File.join(new_image_dir, "original#{File.extname(image)}")
  FileUtils::mkdir new_image_dir
  FileUtils::cp image, new_image_path
  puts "new  image path: #{new_image_path}"
  FileUtils::rm image
end


def search_children path
  search_pattern = File.join(path, "*")
  Dir[search_pattern]
end

start
