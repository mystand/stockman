# Load the Sinatra app
require File.join(File.dirname(__FILE__), '..', 'stockman')

# Load the testing libraries
require 'rspec'
#require 'spec/interop/test'
require 'rack/test'

# Set the Sinatra environment
set :environment, :test

RSpec.configure do |conf|
  conf.include Rack::Test::Methods
end

# Add an app method for RSpec
def app
  Sinatra::Application
end