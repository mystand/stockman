require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

describe 'Stockman service' do
  let :client_key do
    Digest::MD5.hexdigest admin_key
  end

  let :project do
    "test_project"
  end

  let :admin_key do
    "test_admin_key"
  end

  let :project_dir do
    File.join public_dir, project
  end

  let :public_dir do
    File.join File.dirname(__FILE__), "..", "public"
  end

  let :test_image do
    file_path = File.new(File.join(File.dirname(__FILE__), "assets", "test.jpg"))
    Rack::Test::UploadedFile.new(file_path, "image/jpeg")
  end

  let :test_image_png do
    file_path = File.new(File.join(File.dirname(__FILE__), "assets", "test.png"))
    Rack::Test::UploadedFile.new(file_path, "image/png")
  end

  after :each do
    FileUtils.rm_rf project_dir
  end

  it "should success save, open and delete image" do
    post "/#{project}/#{admin_key}", "files[0]" => test_image
    last_response.should be_ok
  end

  it "should open image" do
    post "/#{project}/#{admin_key}", "files[0]" => test_image
    last_response.should be_ok
    get "/#{project}/#{client_key}"
    last_response.should be_redirect
    get last_response.header["Location"]
    last_response.should be_ok
  end

  it "should save and delete image" do
    post "/#{project}/#{admin_key}", "files[0]" => test_image
    last_response.should be_ok
    delete "/#{project}/#{admin_key}/all"
    last_response.should be_ok
  end

  it "should render image gallery with correct formats" do
    post "/#{project}/#{admin_key}", "files[0]" => test_image
    last_response.should be_ok
    post "/#{project}/#{admin_key}", "files[1]" => test_image_png
    last_response.should be_ok
    get "/#{project}/#{client_key}/all"
    last_response.should be_ok
    result = JSON.parse(last_response.body)
    result.size.should equal(2)
    post "/#{project}/#{admin_key}", "files[2]" => test_image
    last_response.should be_ok
    get "/#{project}/#{client_key}/all"
    last_response.should be_ok
    result = JSON.parse(last_response.body)
    result.size.should equal(3)
    #TODO понять почему разная последовательность у файлов
    ext_list = result.map{|r| File.extname(r)}
    ext_list.should include(".jpg")
    ext_list.should include(".png")
  end


  it "should open image with format 100x100" do
    post "/#{project}/#{admin_key}", "files[0]" => test_image
    last_response.should be_ok
    get "/#{project}/#{client_key}/100x100"
    last_response.should be_redirect
    get last_response.header["Location"]
    last_response.should be_ok
  end

  it "should open image gallery with format 100x100" do
    post "/#{project}/#{admin_key}", "files[0]" => test_image,
         "files[1]" => test_image
    last_response.should be_ok
    get "/#{project}/#{client_key}/all/100x100"
    last_response.should be_ok
    result = JSON.parse(last_response.body)
    result.size.should equal(2)
    url = result[0]
    File.basename(url).should eq("100x100.jpg")
    get "/#{project}/#{client_key}/#{url}"
    last_response.should be_redirect
    get last_response.header["Location"]
    last_response.should be_ok
  end

  it "should open image from gallery" do
    post "/#{project}/#{admin_key}", "files[0]" => test_image
    last_response.should be_ok
    get "/#{project}/#{client_key}/all"
    last_response.should be_ok
    result = JSON.parse(last_response.body)
    full_image_path = result[0]
    File.basename(full_image_path).should eq("original.jpg")
    get "/#{project}/#{client_key}/#{full_image_path}"
    last_response.should be_ok
  end

  it "should delete image from gallery" do
    post "/#{project}/#{admin_key}", "files[0]" => test_image
    last_response.should be_ok
    get "/#{project}/#{client_key}/all"
    last_response.should be_ok
    result = JSON.parse(last_response.body)
    full_image_path = result[0]
    uid = full_image_path.split("/")[0]
    delete "/#{project}/#{admin_key}/#{uid}"
    last_response.should be_ok
    get "/#{project}/#{client_key}/#{full_image_path}"
    last_response.status.should equal(404)
  end
end