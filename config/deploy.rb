lock '3.2.1'
set :application, 'stockman'
set :repo_url, 'git@bitbucket.org:mystand/stockman.git'
set :deploy_to, "/srv/mystand/#{fetch(:application)}/repository"
set :shared, "#{fetch(:deploy_to)}/shared"

set :linked_files, []
set :linked_dirs, %w{public}

set :rvm_type, :system
set :rvm_ruby_version, '2.1.0'

set :unicorn_config_path, "#{fetch(:shared)}/config/unicorn.rb"
set :unicorn_pid, "#{fetch(:shared)}/pids/unicorn.pid"

set :bundle_path, "#{fetch(:shared)}/bundle"

after 'deploy:finishing', 'unicorn:restart'

# slack
set :slack_team, "mystand"
set :slack_token, "AV0JgaewSSQ8kPrqIDarS3oi"
set :slack_channel,      ->{ '#stockman' }
set :slack_icon_emoji, -> { ":see_no_evil:" }