window.Stockman = class Stockman
  constructor: (@el, @url, @uuid) ->
    @dragDropField = document.createElement 'div'
    @dragDropField.innerHTML = "Drag'n'drop or Click"
    @dragDropField.className = 'drag-n-drop'
    @dragDropField.addEventListener 'drop', @drop
    @dragDropField.addEventListener 'dragover', @dragover
    @dragDropField.addEventListener 'dragenter', @dragenter
    @dragDropField.addEventListener 'dragleave', @dragleave
    @dragDropField.addEventListener 'click', @clickOnDrag

    @input = document.createElement 'input'
    @input.type = 'file'
    @input.multiple = 'multiple'
    @input.addEventListener 'change', @upload

    @el.appendChild @dragDropField
    @el.appendChild @input

  clickOnDrag: (ev) =>
    @input.click()

  dragover: (ev) =>
    ev.preventDefault();

  dragenter: (ev) =>
    @addClass ev.currentTarget, 'hover'

  dragleave: (ev) =>
    @removeClass ev.currentTarget, 'hover'

  drop: (ev) =>
    ev.preventDefault();
    @removeClass ev.currentTarget, 'hover'
    @send ev.dataTransfer.files

  upload: =>
    @send @input.files

  send: (files) =>
    xhr = new XMLHttpRequest;
    xhr.open 'post', @url, true
    xhr.onload = @onLoad
    formData = new FormData
    formData.append "files[#{i}]", file for file, i in files
    formData.append 'uuid', @uuid
    xhr.send formData

  onLoad: =>
    for img in @el.querySelectorAll('img')
      src = img.src.split '?'
      img.src = src[0] + '?' + Math.random()

  @adminKey: (uuid) ->
    CryptoJS.MD5(uuid).toString()

  @clientKey: (uuid) ->
    CryptoJS.MD5(Stockman.adminKey(uuid)).toString()

# private
  addClass: (el, klass) =>
    if el.className.indexOf(klass) < 0
      el.className = "#{el.className} #{klass}"

  removeClass: (el, klass) =>
    if el.className.indexOf(klass) > -1
      newClass = " #{el.className} ".replace(" #{klass} ", " ")
      el.className = newClass.substring(1, newClass.length - 1)


window.onload = ->
  for el in document.querySelectorAll('.stockman')
    url = el.getAttribute 'data-url'
    uuid = el.getAttribute 'data-uuid'
    new Stockman el, url, uuid
