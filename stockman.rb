require 'sinatra'
require 'sinatra/cross_origin'
require 'sinatra/reloader'
#require "sinatra/reloader" if development?  #TODO doesn't works
require 'json'

require './image_processor'

configure do
  enable :cross_origin
  set :allow_methods, [:get, :post, :delete, :put, :options]
end


before do
  halt 200 if request.request_method == 'OPTIONS'
end

# POST /:project/:admin_key
post /\/(.+)\/(.+)/ do |project, admin_key|
  content_type :json
  client_key = Digest::MD5.hexdigest admin_key
  directory = image_directory(project, client_key)
  FileUtils::mkdir_p directory

  files = params[:files].map do |i, file|
    tempfile = file[:tempfile]
    extname = File.extname file[:filename]
    file_uid = SecureRandom.hex
    FileUtils::mkdir_p File.join(directory, file_uid)
    filepath = File.join(directory, file_uid, "#{default_format}#{extname}")
    FileUtils::cp tempfile.path, filepath
    filepath
  end
  files.to_json
end

# GET /:project/:client_key/all/:format
get /\/([^\/]+)\/([^\/]+)\/all\/?(.*)/ do |project, client_key, format|
  content_type :json
  files = get_images(project: project, client_key: client_key, format: format)
  files.to_json
end

## GET /:project/:client_key/:uid/:format
get /\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)/ do |project, client_key, uid, format|
  image_path = get_or_format_image(project: project, client_key: client_key, format: format, uid: uid)
  if image_path.nil?
    status 404
  else
    redirect request.path
  end
end

# GET /:project/:client_key/:format
get /\/([^\/]+)\/([^\/]+)\/?(.*)/ do |project, client_key, format|
  image_path = get_or_format_image(project: project, client_key: client_key, format: format)
  if image_path.nil?
    status 404
  else
    redirect File.join(project, client_key, image_path)
  end
end

# DELETE /:project/:admin_key/all
delete /\/(.+)\/(.+)\/all/ do |project, admin_key|
  client_key = Digest::MD5.hexdigest admin_key
  dir = File.join image_directory(project, client_key)
  FileUtils.rm_rf(dir)
end

# DELETE /:project/:admin_key/:file_uid
delete /\/(.+)\/(.+)\/(.+)/ do |project, admin_key, file_uid|
  client_key = Digest::MD5.hexdigest admin_key
  image_dir = File.join image_directory(project, client_key), file_uid
  FileUtils.rm_rf image_dir
end

private

def get_images options
  project = options[:project]
  client_key = options[:client_key]
  format = options[:format]
  format = default_format if format.size.zero?
  search_pattern = File.join image_directory(project, client_key), '*'
  uid_dirs = Dir[search_pattern].sort_by { |f| File.stat(f).mtime }.map { |d| File.basename(d) }
  uid_dirs.map do |uid|
    original_file = search_file options.merge({format: default_format, uid: uid})
    res = nil
    if original_file
      filename = "#{format}#{File.extname(original_file)}"
      res = escape_format File.join(uid, filename)
    end
    res
  end.compact
end

def get_or_format_image options
  format = options[:format]
  format = default_format if format.size.zero?
  format = format.split(".")[0]
  res = search_file options.merge({format: format})
  if res.nil?
    project = options[:project]
    client_key = options[:client_key]
    uid = options[:uid] || last_file_uid(project, client_key)
    return nil unless uid
    original = search_file options.merge(format: default_format)
    return nil unless original
    formatted_filename = "#{format}#{File.extname(original)}"
    if format!= default_format
      formatted_file_path = File.join image_directory(project, client_key), uid, formatted_filename
      file = File.join(public_directory, project, client_key, original)
      formatted_file = ImageProcessor.apply file, format
      formatted_file.write formatted_file_path
    end
    res = File.join(uid, formatted_filename)
  end
  escape_format res
end

def escape_format str
  return str
  format = str.split("/").last.split(".").reverse[1]
  base_url, ext = str.split(format)
  format = URI.escape format
  [base_url, format, ext].join ""
end

def search_file options
  uid = options[:uid]
  format = options[:format]
  project = options[:project]
  client_key = options[:client_key]
  uid = last_file_uid project, client_key unless uid
  return nil unless uid
  search_pattern = File.join(image_directory(project, client_key), uid, "#{format}.*")
  file = Dir[search_pattern].last
  File.join(uid, File.basename(file)) if file # file url
end

def last_file_uid project, client_key
  search_pattern = File.join image_directory(project, client_key), '*'
  dir_path = Dir[search_pattern].sort_by { |f| File.stat(f).mtime }.last
  File.basename(dir_path) if dir_path
end

def image_directory(project, client_key)
  File.join public_directory, project, client_key
end


def public_directory
  File.join File.dirname(__FILE__), 'public'
end

def default_format
  "original"
end